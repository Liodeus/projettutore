$(document).ready(function() {
    // Configuration fullPage
    $('#fullpage').fullpage({
        sectionsColor : ['#f2f2f2','#f2f2f2', '#f2f2f2', '#f2f2f2', '#f2f2f2'],
        anchors: ['home', 'level2', 'level1', 'level0', 'level-1'],
        menu: '#menu',
        loopHorizontal: false
    });

    // Affichage voitures
    $('.car').hide();

    // Affichage compteurs map
    $('#lvl2Ct').text($('#lvl2 img.car:visible').length + '/40');
    $('#lvl1Ct').text($('#lvl1 img.car:visible').length + '/40');
    $('#lvl0Ct').text($('#lvl0 img.car:visible').length + '/40');
    $('#lvl-1Ct').text($('#lvl-1 img.car:visible').length + '/120');
    $('#globalCt').text($('img.car:visible').length + '/240');

    $('.slot').on('click', function() {
       
        $(this).children('img').toggle()

        // Raffraichissement compteurs map
        $('#lvl2Ct').text($('#lvl2 img.car:visible').length + '/40');
        $('#lvl1Ct').text($('#lvl1 img.car:visible').length + '/40');
        $('#lvl0Ct').text($('#lvl0 img.car:visible').length + '/40');
        $('#lvl-1Ct').text($('#lvl-1 img.car:visible').length + '/120');
        $('#globalCt').text($('img.car:visible').length + '/240');
    });

    // Click sur sortie
    $('.door').on('click', function() {

        // Actualisation des sorties
        switch($(this).children('img').attr('id')) {
            case 'e1':
            case 'se1':
                if ($(this).children('img').attr('src') == 'ressources/exitGreen.png') 
                        {
                            $('#e1').attr('src', 'ressources/exitRed.png'); // Remplacer par rouge
                            $('#se1').attr('src', 'ressources/exitRed.png'); // Remplacer par rouge
                        }
                        else
                        {
                            $('#e1').attr('src', 'ressources/exitGreen.png'); // Remplacer par vert
                            $('#se1').attr('src', 'ressources/exitGreen.png'); // Remplacer par vert
                        }
                break;

            case 'e0':
            case 'se0':
                if ($(this).children('img').attr('src') == 'ressources/exitGreen.png') 
                        {
                            $('#e0').attr('src', 'ressources/exitRed.png'); // Remplacer par rouge
                            $('#se0').attr('src', 'ressources/exitRed.png'); // Remplacer par rouge
                        }
                        else
                        {
                            $('#e0').attr('src', 'ressources/exitGreen.png'); // Remplacer par vert
                            $('#se0').attr('src', 'ressources/exitGreen.png'); // Remplacer par vert
                        }
                break;

            case 'e-1':
            case 'se-1':
                if ($(this).children('img').attr('src') == 'ressources/exitGreen.png') 
                        {
                            $('#e-1').attr('src', 'ressources/exitRed.png'); // Remplacer par rouge
                            $('#se-1').attr('src', 'ressources/exitRed.png'); // Remplacer par rouge
                        }
                        else
                        {
                            $('#e-1').attr('src', 'ressources/exitGreen.png'); // Remplacer par vert
                            $('#se-1').attr('src', 'ressources/exitGreen.png'); // Remplacer par vert
                        }
                break;
        }

        // Algorithme de la plus proche sortie
        var exitState = new Array(); 

        // Stockage de l'état des sorties dans le tableau exitState
        if ($('#se1').attr('src') == 'ressources/exitGreen.png') // Niveau 1
            exitState[1] = 1;
        else
            exitState[1] = 0;

        if ($('#se0').attr('src') == 'ressources/exitGreen.png') // Niveau 0
            exitState[0] = 1;
        else
            exitState[0] = 0;

        if ($('#se-1').attr('src') == 'ressources/exitGreen.png') // Niveau -1
            exitState[-1] = 1;
        else
            exitState[-1] = 0;

        // Affichage des flèches
        if (exitState[1] == 0)  // 0XX
        {
            if (exitState[0] == 0) // 00X
            {
                if (exitState[-1] == 0) // 000
                {
                    alert("Warning : There is no more exit open !");
                    $('#arrow1').attr('src', 'ressources/cross.png');
                    $('#arrow0').attr('src', 'ressources/cross.png');
                    $('#arrow-1').attr('src', 'ressources/cross.png');
                }
                else // 001
                {
                    $('#arrow1').attr('src', 'ressources/downArrow.png');
                    $('#arrow0').attr('src', 'ressources/downArrow.png');
                    $('#arrow-1').attr('src', 'ressources/rightArrow.png');
                }
            }
            else // 01X
            {
                if (exitState[-1] == 0) // 010
                {
                    $('#arrow1').attr('src', 'ressources/downArrow.png');
                    $('#arrow0').attr('src', 'ressources/rightArrow.png');
                    $('#arrow-1').attr('src', 'ressources/upArrow.png');
                }
                else // 011
                {
                    $('#arrow1').attr('src', 'ressources/downArrow.png');
                    $('#arrow0').attr('src', 'ressources/rightArrow.png');
                    $('#arrow-1').attr('src', 'ressources/rightArrow.png');
                }
            }
        }
        else // 1XX
        {
            if (exitState[0] == 0) // 10X
            {
                if (exitState[-1] == 0) // 100
                {
                    $('#arrow1').attr('src', 'ressources/rightArrow.png');
                    $('#arrow0').attr('src', 'ressources/upArrow.png');
                    $('#arrow-1').attr('src', 'ressources/upArrow.png');
                }
                else // 101
                {
                    $('#arrow1').attr('src', 'ressources/rightArrow.png');
                    $('#arrow0').attr('src', 'ressources/upArrow.png');
                    $('#arrow-1').attr('src', 'ressources/rightArrow.png');
                }
            }
            else // 11X
            {
                if (exitState[-1] == 0) // 110
                {
                    $('#arrow1').attr('src', 'ressources/rightArrow.png');
                    $('#arrow0').attr('src', 'ressources/rightArrow.png');
                    $('#arrow-1').attr('src', 'ressources/upArrow.png');
                }
                else // 111
                {
                    $('#arrow1').attr('src', 'ressources/rightArrow.png');
                    $('#arrow0').attr('src', 'ressources/rightArrow.png');
                    $('#arrow-1').attr('src', 'ressources/rightArrow.png');
                }
            }
        }
    });

    // Liens map
    $('.map').on('click', function() {
        switch ($(this).attr('id')) {
            case 'mapLvl2':
                window.location.href = '#level2';
                break;

            case 'mapLvl1':
                window.location.href = '#level1';
                break;

            case 'mapLvl0':
                window.location.href = '#level0';
                break;

            case 'mapLvl-1':
                window.location.href = '#level-1';
                break;
        }
    });
});