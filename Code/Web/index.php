<!DOCTYPE html>
<html>
<head>
    <title>Parking</title>
    <meta charset="utf-8"/>
    <meta name="author" content="Thomas Henrissat"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- styles -->
    <link rel="stylesheet" href="css/normalize.css"/>
    <link rel="stylesheet" href="css/jQuery.fullPage.css"/>
    <link rel="stylesheet" href="css/bootstrap.css"/>
    <link rel="stylesheet" href="css/style.css"/>
</head>

<body>
	<div class="container-fluid">
        <!-- Menu -->
        <ul id="menu">
            <li data-menuanchor="home"><a href="#home">Home</a></li>
            <li data-menuanchor="level2"><a href="#level2">Level 2</a></li>
            <li data-menuanchor="level1"><a href="#level1">Level 1</a></li>
            <li data-menuanchor="level0"><a href="#level0">Level 0</a></li>
            <li data-menuanchor="level-1"><a href="#level-1">Level -1</a></li>
        </ul>
        <!-- End Menu -->

        <div id="fullpage">
            <!-- Home -->
            <div class="section" data-anchor="home">
                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4"></div> <!-- Offset -->
                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" id="building">
                    <!-- Level 2 -->
                    <div class="row rowMap">
                        <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">
                            <div class="map" id="mapLvl2">
                                <h2>Level 2</h2>
                                <h3 id="lvl2Ct">ct</h3>
                            </div>
                        </div>

                        <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5 globalCt">
                            <h2>Global</h2>
                            <h3 id="globalCt">ct</h3>
                        </div>
                    </div>

                    <!-- Level 1 -->
                    <div class="row rowMap">
                        <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">
                            <div class="map" id="mapLvl1">
                                <h2>Level 1</h2>
                                <h3 id="lvl1Ct">ct</h3>
                            </div>
                        </div>

                        <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
                            <div class="door">
                                <img src="ressources/exitGreen.png" height="60" width="60" class="sExit" id="se1">
                            </div>
                        </div>
                    </div>

                    <!-- Level 0 -->
                    <div class="row rowMap">
                        <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">
                            <div class="map" id="mapLvl0">
                                <h2>Level 0</h2>
                                <h3 id="lvl0Ct">ct</h3>
                            </div>
                        </div>

                        <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
                            <div class="door">
                                <img src="ressources/exitGreen.png" height="60" width="60" class="sExit" id="se0">
                            </div>
                        </div>
                    </div>
                    
                    <!-- Level -1 -->
                    <div class="row rowMap">
                        <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">
                            <div class="map" id="mapLvl-1">
                                <h2>Level -1</h2>
                                <h3 id="lvl-1Ct">ct</h3>
                            </div>
                        </div>

                        <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5" id="globalCt">
                            <div class="door">
                                <img src="ressources/exitGreen.png" height="60" width="60" class="sExit" id="se-1">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Home -->

            <!-- Level 2 -->
            <div class="section" data-anchor="level2" id="lvl2">
                <?php
                    for ($i = 1; $i <= 4; $i++) { // Affichage des 4 lignes
                        
                        if ($i == 3) { // Affichage de la route
                                echo'
                                    <!-- Road -->
                                    <div class="row rowRoad">
                                        <img src="ressources/downArrow.png" height="70" width="70" class="arrow">
                                    </div>';
                            }

                        echo '
                            <!-- Row '.$i.' -->
                            <div class="row rowSlot">
                                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2"></div>  <!-- Offset -->';

                            for ($j = 1; $j <= 10; $j++) { // Affichage des 10 places
                                echo '
                                    <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                                        <div class="slot">
                                            <img src="ressources/car.png" height="70" width="70" class="car">
                                        </div>
                                    </div>';
                            }
                        echo '</div>';
                    }
                ?>
            </div>

            <!-- Level 1 -->
            <div class="section" data-anchor="level1" id="lvl1">
                <?php
                    for ($i = 1; $i <= 4; $i++) { // Affichage des 4 lignes
                        
                        if ($i == 3) { // Affichage de la route
                                echo'
                                    <!-- Road -->
                                    <div class="row">
                                        <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5"></div>
                                        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                                            <img src="ressources/rightArrow.png" height="70" width="70" class="arrow" id="arrow1">
                                        </div>
                                        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2"></div>
                                        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                                            <div class="door">
                                                <img src="ressources/exitGreen.png" height="80" width="80" class="exit" id="e1">
                                            </div>
                                        </div>
                                    </div>';
                            }

                        echo '
                            <!-- Row '.$i.' -->
                            <div class="row rowSlot">
                                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2"></div> <!-- Offset -->';

                            for ($j = 1; $j <= 10; $j++) { // Affichage des 10 places
                                echo '
                                    <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                                        <div class="slot">
                                            <img src="ressources/car.png" height="70" width="70" class="car">
                                        </div>
                                    </div>';
                            }
                        echo '</div>';
                    }
                ?>
            </div>

            <!-- Level 0 -->
            <div class="section" data-anchor="level0" id="lvl0">
                <?php
                    for ($i = 1; $i <= 4; $i++) { // Affichage des 4 lignes
                        
                        if ($i == 3) { // Affichage de la route
                                echo'
                                    <!-- Road -->
                                    <div class="row">
                                        <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5"></div>
                                        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                                            <img src="ressources/rightArrow.png" height="70" width="70" class="arrow" id="arrow0">
                                        </div>
                                        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2"></div>
                                        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                                            <div class="door">
                                                <img src="ressources/exitGreen.png" height="80" width="80" class="exit" id="e0">
                                            </div>
                                        </div>
                                    </div>';
                            }

                        echo '
                            <!-- Row '.$i.' -->
                            <div class="row rowSlot">
                                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2"></div> <!-- Offset -->';

                            for ($j = 1; $j <= 10; $j++) { // Affichage des 10 places
                                echo '
                                    <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                                        <div class="slot">
                                            <img src="ressources/car.png" height="70" width="70" class="car">
                                        </div>
                                    </div>';
                            }
                        echo '</div>';
                    }
                ?>
            </div>

            <!-- Level -1 -->
          	<div class="section" data-anchor="level-1" id="lvl-1">
                <div class="slide" id="level-1A">
                    <?php
                        for ($i = 1; $i <= 4; $i++) { // Affichage des 4 lignes
                            
                            if ($i == 3) { // Affichage de la route
                                    echo'
                                        <!-- Road -->
                                        <div class="row rowRoad">
                                            <img src="ressources/rightArrow.png" height="70" width="70" class="arrow">
                                        </div>';
                                }

                            echo '
                                <!-- Row '.$i.' -->
                                <div class="row rowSlot">
                                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2"></div> <!-- Offset -->';

                                for ($j = 1; $j <= 10; $j++) { // Affichage des 10 places
                                    echo '
                                        <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                                            <div class="slot">
                                                <img src="ressources/car.png" height="70" width="70" class="car">
                                            </div>
                                        </div>';
                                }
                            echo '</div>';
                        }
                    ?>
                </div>

                <div class="slide active" id="level-1B">
                    <?php
                        for ($i = 1; $i <= 4; $i++) { // Affichage des 4 lignes
                            
                            if ($i == 3) { // Affichage de la route
                                    echo'
                                        <!-- Road -->
                                        <div class="row">
                                            <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5"></div>
                                            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                                                <img src="ressources/rightArrow.png" height="70" width="70" class="arrow" id="arrow-1">
                                            </div>
                                            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2"></div>
                                            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                                                <div class="door">
                                                    <img src="ressources/exitGreen.png" height="80" width="80" class="exit" id="e-1">
                                                </div>
                                            </div>
                                        </div>';
                                }

                            echo '
                                <!-- Row '.$i.' -->
                                <div class="row rowSlot">
                                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2"></div> <!-- Offset -->';

                                for ($j = 1; $j <= 10; $j++) { // Affichage des 10 places
                                    echo '
                                        <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                                            <div class="slot">
                                                <img src="ressources/car.png" height="70" width="70" class="car">
                                            </div>
                                        </div>';
                                }
                            echo '</div>';
                        }
                    ?>
                </div>

                <div class="slide" id="level-1C">
                    <?php
                        for ($i = 1; $i <= 4; $i++) { // Affichage des 4 lignes
                            
                            if ($i == 3) { // Affichage de la route
                                    echo'
                                        <!-- Road -->
                                        <div class="row rowRoad">
                                            <img src="ressources/leftArrow.png" height="70" width="70" class="arrow">
                                        </div>';
                                }

                            echo '
                                <!-- Row '.$i.' -->
                                <div class="row rowSlot">
                                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2"></div> <!-- Offset -->';

                                for ($j = 1; $j <= 10; $j++) { // Affichage des 10 places
                                    echo '
                                        <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                                            <div class="slot">
                                                <img src="ressources/car.png" height="70" width="70" class="car">
                                            </div>
                                        </div>';
                                }
                            echo '</div>';
                        }
                    ?>
                </div>
           	</div>
   	    </div>
   </div> <!-- End Container-fluid -->

   <!-- scripts -->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="script/jQuery.fullPage.js"></script>
    <script src="script/jQuery.script.js"></script>
</body>
</html>